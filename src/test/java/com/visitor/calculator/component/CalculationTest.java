package com.visitor.calculator.component;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculationTest {
    private Calculation calculation;

    @BeforeEach
    private void setUp(){
        calculation = new CalculationImpl();
    }

    @Test
    void calculate_bmi_result_test() {
        var bmi = new Bmi(85D,1.78D);
        double result = calculation.calculate(bmi);
        assertThat(result).isEqualTo(26.83);
    }

    @Test
    void  calculate_bmi_height_equals_zero(){
        var bmiHeightZero = new Bmi(1.78D,0D);
        assertThatThrownBy (() -> calculation.calculate(bmiHeightZero))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage(null);
    }

    @Test
    void  calculate_bmi_mass_equals_zero(){
        var bmiMassZero = new Bmi(0D,1.78);
        assertThrows(IllegalArgumentException.class, () ->{
            calculation.calculate(bmiMassZero);
        });
    }

    @Test
    void calculate_tax_result() {
        var tax  = new Tax(100D,100D,1);
        double result = calculation.calculate(tax);
        assertThat(result).isEqualTo(100.18);
    }
}