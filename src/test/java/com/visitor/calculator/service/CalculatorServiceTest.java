package com.visitor.calculator.service;

import com.visitor.calculator.dto.BmiCalculatorDto;
import com.visitor.calculator.dto.TaxCalculatorDto;
import com.visitor.calculator.enums.CalculatorType;
import com.visitor.calculator.mapstruct.CalculationMapstruct;
import com.visitor.calculator.model.calculator.CalculatorResultResponse;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;
import com.visitor.calculator.repository.CalculationRepository;
import com.visitor.calculator.service.impl.CalculatorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CalculatorServiceTest {

    @Mock /*** @Mock - you are creating a complete mock or fake object ***/
    private CalculationRepository calcRepository;
    @Spy /*** @Spy - is the real object and you just spying or stubbing specific methods of it. ***/
    private CalculationMapstruct calcMapstruct;

    @InjectMocks
    private CalculatorServiceImpl calcService;
    BmiCalculatorDto bmiCalculatorDto;
    TaxCalculatorDto taxCalculatorDto;
    Calculator bmiCalc;
    Calculator taxCalc;
    CalculatorResultResponse calculatorResultResponseBmi;
    CalculatorResultResponse calculatorResultResponseTax;
    CalculatorResponse bmiCalculatorResponse;
    CalculatorResponse taxCalculatorResponse;





    @BeforeEach
    public void setUp(){
        bmiCalculatorDto = BmiCalculatorDto.builder()
                .height(1.78D)
                .mass(80D)
                .type("bmi")
                .visitorId(1L)
                .build();
        taxCalculatorDto = TaxCalculatorDto.builder()
                .price(100D)
                .taxRate(10D)
                .additionalTax(1)
                .type("tax")
                .visitorId(1L)
                .build();
        bmiCalc = Calculator.builder()
                .id(2L)
                .result(26.83)
                .type(CalculatorType.bmi)
                .visitor(null)
                .build();
        calculatorResultResponseBmi = CalculatorResultResponse.builder()
                .calculatorType(CalculatorType.bmi)
                .result(25.25)
                .build();
        calculatorResultResponseTax = CalculatorResultResponse.builder()
                .calculatorType(CalculatorType.tax)
                .result(100.18)
                .build();
        taxCalc = Calculator.builder()
                .id(3L)
                .result(100.18)
                .type(CalculatorType.tax)
                .visitor(null)
                .createDateTime(LocalDateTime.parse("2022-07-25T22:52:34.640187"))
                .updateDateTime(LocalDateTime.parse("2022-07-25T22:52:34.640209"))
                .build();
        bmiCalculatorResponse = CalculatorResponse.builder()
                .result(26.83)
                .type(CalculatorType.bmi)
                .build();
        taxCalculatorResponse = CalculatorResponse.builder()
                .result(100.18)
                .type(CalculatorType.tax)
                .build();
    }


    @Test
    void calculate_bmi() {
        /*** Arrange ***/
        when(calcMapstruct.mapToBmiResponse(bmiCalculatorDto)).thenReturn(calculatorResultResponseBmi);
        when(calcMapstruct.mapBmiCalculatorDtoToCalculator(bmiCalculatorDto, calculatorResultResponseBmi)).thenReturn(bmiCalc);
        when(calcRepository.save(bmiCalc)).thenReturn(bmiCalc);
        when(calcMapstruct.mapToCalculatorResponse(bmiCalc)).thenReturn(bmiCalculatorResponse);

        /*** Act ***/
        CalculatorResponse response = calcService.calculate(bmiCalculatorDto);

        /*** Assertion ***/
        assertThat(response.getType()).isEqualTo(CalculatorType.bmi);

        verify(calcMapstruct,times(1)).mapToBmiResponse(bmiCalculatorDto);
        verify(calcMapstruct, times(1)).mapBmiCalculatorDtoToCalculator(bmiCalculatorDto, calculatorResultResponseBmi);
        verify(calcRepository, times(1)).save(bmiCalc);
        verify(calcMapstruct, times(1)).mapToCalculatorResponse(bmiCalc);
    }

    @Test
    void calculate_tax() {
        /***Arrange***/
        when(calcMapstruct.mapToTaxResponse(taxCalculatorDto)).thenReturn(calculatorResultResponseTax);
        when(calcMapstruct.mapTaxCalculatorDtoToCalculator(taxCalculatorDto,calculatorResultResponseTax)).thenReturn(taxCalc);
        when(calcRepository.save(taxCalc)).thenReturn(taxCalc);
        when(calcMapstruct.mapToCalculatorResponse(taxCalc)).thenReturn(taxCalculatorResponse);
        /***Act***/
        CalculatorResponse response = calcService.calculate(taxCalculatorDto);

        /*** Assertion ***/
        assertThat(response.getType()).isEqualTo(CalculatorType.tax);

        verify(calcMapstruct,times(1)).mapToTaxResponse(taxCalculatorDto);
        verify(calcMapstruct, times(1)).mapTaxCalculatorDtoToCalculator(taxCalculatorDto,calculatorResultResponseTax);
        verify(calcRepository, times(1)).save(taxCalc);
        verify(calcMapstruct, times(1)).mapToCalculatorResponse(taxCalc);
    }
}