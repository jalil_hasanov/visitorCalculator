package com.visitor.calculator.service;

import com.visitor.calculator.dto.VisitorDto;
import com.visitor.calculator.mapstruct.VisitorMapStruct;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.model.visitor.VisitorResponse;
import com.visitor.calculator.repository.VisitorRepository;
import com.visitor.calculator.service.impl.VisitorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VisitorServiceImplTest {

    @Mock
    private VisitorRepository visitorRepository;
    @Mock
    private VisitorMapStruct visitorMapStruct;
    @InjectMocks
    private VisitorServiceImpl visitorService;
    Visitor visitor;
    VisitorDto visitorDto;

    VisitorResponse visitorResponse;

    @BeforeEach
    void setUp(){
        visitorDto = VisitorDto.builder()
                .username("admin1")
                .mail("admin2@gmail.com")
                .password("root")
                .build();
        visitor = Visitor.builder()
                .id(1L)
                .username("admin1")
                .mail("admin2@gmail.com")
                .password("root")
                .createDateTime(LocalDateTime.now())
                .updateDateTime(LocalDateTime.now())
                .build();
        visitorResponse = VisitorResponse.builder()
                .calculations(null)
                .mail("admin2@gmail.com")
                .username("admin1")
                .build();
    }

    @Test
    void create() {
        /*** Arrange ***/
        when(visitorMapStruct.visitorDtoToVisitor(visitorDto)).thenReturn(visitor);
        when(visitorRepository.save(visitor)).thenReturn(visitor);
        when(visitorMapStruct.mapVisitorToVisitorResponse(visitor)).thenReturn(visitorResponse);

        /*** Act ***/
        VisitorResponse response =  visitorService.create(visitorDto);

        /*** Assertion ***/
        assertThat(visitor.getUsername()).isEqualTo("admin1");

        verify(visitorMapStruct, timeout(2)).visitorDtoToVisitor(visitorDto);
        verify(visitorRepository, timeout(1)).save(visitor);
    }

}