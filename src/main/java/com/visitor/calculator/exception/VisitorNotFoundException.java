package com.visitor.calculator.exception;

public class VisitorNotFoundException extends RuntimeException{
    public VisitorNotFoundException() {
        this("Visitor Not Found");
    }

    public VisitorNotFoundException(String message) {
        super(message);
    }
}
