package com.visitor.calculator.enums;

public enum CalculatorType {
    bmi("bmi"), tax("tax");

    private String value;

    CalculatorType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static CalculatorType fromString(String text) {
        for (CalculatorType c : CalculatorType.values()) {
            if (c.value.equalsIgnoreCase(text)) {
                return c;
            }
        }
        return null;
    }
}
