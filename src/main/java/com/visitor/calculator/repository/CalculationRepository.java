package com.visitor.calculator.repository;

import com.visitor.calculator.model.calculator.Calculator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculationRepository extends JpaRepository<Calculator,Long> {

}
