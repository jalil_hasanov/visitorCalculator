package com.visitor.calculator.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.visitor.calculator.component")
public class CalculationConfig { }