package com.visitor.calculator.mapstruct;

import com.visitor.calculator.dto.VisitorDto;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.model.visitor.VisitorResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {CalculationMapstruct.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class VisitorMapStruct {
    @Autowired
    CalculationMapstruct calculationMapstruct;
    public abstract Visitor visitorDtoToVisitor(VisitorDto visitor);
    @Mapping(target = "calculations", source = "createdVisitor.calculations", qualifiedByName = "toCalcResponseList")
    public abstract VisitorResponse mapVisitorToVisitorResponse(Visitor createdVisitor);

    @Named("toCalcResponseList")
    protected List<CalculatorResponse>toCalcResponseList(List<Calculator>calculators){
        return (CollectionUtils.isEmpty(calculators)) ? null : calculators.stream()
                .map(c -> calculationMapstruct.mapToCalculatorResponse(c))
                .collect(Collectors.toList());
    }
}
