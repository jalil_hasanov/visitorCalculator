package com.visitor.calculator.mapstruct;

import com.visitor.calculator.component.Bmi;
import com.visitor.calculator.component.Calculation;
import com.visitor.calculator.component.Tax;
import com.visitor.calculator.dto.BmiCalculatorDto;
import com.visitor.calculator.dto.TaxCalculatorDto;
import com.visitor.calculator.enums.CalculatorType;
import com.visitor.calculator.model.calculator.CalculatorResultResponse;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.repository.VisitorRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@Mapper(componentModel = "spring", uses = {VisitorRepository.class, VisitorMapStruct.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class CalculationMapstruct {

    @Autowired
    private VisitorRepository visitorRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    Calculation calculation;

    public CalculationMapstruct(VisitorRepository visitorRepo, ApplicationContext applicationContext, Calculation calculation) {
        this.visitorRepo = visitorRepo;
        this.applicationContext = applicationContext;
        this.calculation = calculation;
    }

    public CalculationMapstruct() {
    }

    @Mapping(target = "result", source = "bmiResult.result")
    @Mapping(target = "type", source = "bmiResult.calculatorType")
    @Mapping(target = "visitor", source = "request.visitorId", qualifiedByName = "getVisitorById")
    public abstract Calculator mapBmiCalculatorDtoToCalculator(BmiCalculatorDto request, CalculatorResultResponse bmiResult);
    @Mapping(target = "result", source = "result.result")
    @Mapping(target = "type", source = "result.calculatorType")
    @Mapping(target = "visitor", source = "request.visitorId", qualifiedByName = "getVisitorById")
    public abstract Calculator mapTaxCalculatorDtoToCalculator(TaxCalculatorDto request, CalculatorResultResponse result);


    @Mapping(target = "tax.price", source = "dto.price")
    @Mapping(target = "tax.taxRate", source = "dto.taxRate")
    @Mapping(target = "tax.additionalTax", source = "dto.additionalTax")
    public abstract Tax mapTaxCalculatorDtoToTax(@MappingTarget Tax tax, TaxCalculatorDto dto);

    public abstract CalculatorResponse mapToCalculatorResponse(Calculator calculator);

    @Mapping(target = "calculatorType", source = "type", qualifiedByName = "fromString")
    @Mapping(target = "result", source = "request", qualifiedByName = "toBmiResult")
    public abstract CalculatorResultResponse mapToBmiResponse(BmiCalculatorDto request);

    @Mapping(target = "calculatorType", source = "request.type", qualifiedByName = "fromString")
    @Mapping(target = "result", source = "request", qualifiedByName = "toTaxResult")
    public abstract CalculatorResultResponse mapToTaxResponse(TaxCalculatorDto request);


    @Named("toBmiResult")
    public Double toBmiResult(BmiCalculatorDto dto){
        Bmi bmi = applicationContext.getBean(Bmi.class);
        bmi.setHeight(dto.getHeight());
        bmi.setMass(dto.getMass());
        Double result = bmi.accept(calculation);
        return result;
    }

    @Named("toTaxResult")
    public Double toTaxResult(TaxCalculatorDto dto){
        Tax tax = (Tax) applicationContext.getBean(dto.getType());
        tax.setAdditionalTax(dto.getAdditionalTax());
        tax.setTaxRate(dto.getTaxRate());
        tax.setPrice(dto.getPrice());
        Double result = tax.accept(calculation);
        return result;
    }




    @Named("getVisitorById")
    public Visitor getVisitorById(Long id){
        return visitorRepo.findById(id).get();
    }

    @Named("fromString")
    public CalculatorType fromString(String s){
        return CalculatorType.fromString(s);
    }


}
