package com.visitor.calculator.model.visitor;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.visitor.calculator.model.calculator.Calculator;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "visitor")
public class Visitor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    Long id;
    @Column(name = "username")
    String username;
    @Column(name = "mail")
    String mail;
    @Column(name = "password")
    String password;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "visitor", targetEntity = Calculator.class)
    @JsonBackReference
    private List<Calculator> calculations;
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createDateTime;
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updateDateTime;
}