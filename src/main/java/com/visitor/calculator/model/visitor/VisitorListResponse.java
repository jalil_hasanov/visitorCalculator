package com.visitor.calculator.model.visitor;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VisitorListResponse {
    List<VisitorResponse>visitors;
}
