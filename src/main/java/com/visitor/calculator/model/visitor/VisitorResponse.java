package com.visitor.calculator.model.visitor;

import com.visitor.calculator.model.calculator.CalculatorResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VisitorResponse {
    String username;
    String mail;
    List<CalculatorResponse> calculations;
}
