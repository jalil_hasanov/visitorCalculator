package com.visitor.calculator.model.calculator;

import com.visitor.calculator.enums.CalculatorType;
import com.visitor.calculator.model.visitor.VisitorResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CalculatorResponse {
    CalculatorType type;
    Double result;
}
