package com.visitor.calculator.model.calculator;

import com.visitor.calculator.enums.CalculatorType;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CalculatorResultResponse {
    CalculatorType calculatorType;
    Double result;
}
