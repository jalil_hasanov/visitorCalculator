package com.visitor.calculator.model.calculator;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.visitor.calculator.enums.CalculatorType;
import com.visitor.calculator.model.visitor.Visitor;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "calculator")
public class Calculator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    Long id;
    @Column(name = "result")
    Double result;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    CalculatorType type;
    @ManyToOne(targetEntity = Visitor.class)
    @JsonManagedReference
    @JoinColumn(name="visitor_id",referencedColumnName = "id",updatable = false, nullable=false)
    Visitor visitor;
    @Column(name = "created_at")
    @CreationTimestamp
    LocalDateTime createDateTime;
    @Column(name = "updated_at")
    @UpdateTimestamp
    LocalDateTime updateDateTime;
}