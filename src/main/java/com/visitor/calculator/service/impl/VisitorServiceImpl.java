package com.visitor.calculator.service.impl;

import com.visitor.calculator.dto.VisitorDto;
import com.visitor.calculator.exception.VisitorNotFoundException;
import com.visitor.calculator.mapstruct.VisitorMapStruct;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.model.visitor.VisitorListResponse;
import com.visitor.calculator.model.visitor.VisitorResponse;
import com.visitor.calculator.repository.VisitorRepository;
import com.visitor.calculator.service.VisitorService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VisitorServiceImpl implements VisitorService {
    private VisitorRepository repository;
    private VisitorMapStruct mapStruct;

    @Override
    public VisitorResponse create(VisitorDto visitorDto) {
        Visitor visitor = mapStruct.visitorDtoToVisitor(visitorDto);
        Visitor createdVisitor = Optional.of(repository.save(visitor)).orElseThrow(VisitorNotFoundException::new);
        return mapStruct.mapVisitorToVisitorResponse(createdVisitor);
    }

    @Override
    public VisitorListResponse findAll() {
        List<VisitorResponse>visitors = repository.findAll().stream()
                .map(mapStruct::mapVisitorToVisitorResponse)
                .collect(Collectors.toList());
        VisitorListResponse response = VisitorListResponse.builder()
                .visitors(visitors)
                .build();
        return response;
    }

    @Override
    public VisitorResponse findById(Long id) {
        Visitor visitor = repository.findById(id).orElseThrow(VisitorNotFoundException::new);
        return mapStruct.mapVisitorToVisitorResponse(visitor);
    }
}
