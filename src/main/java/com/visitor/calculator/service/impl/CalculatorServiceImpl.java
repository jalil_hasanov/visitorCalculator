package com.visitor.calculator.service.impl;

import com.visitor.calculator.component.Calculation;
import com.visitor.calculator.component.Tax;
import com.visitor.calculator.dto.BmiCalculatorDto;
import com.visitor.calculator.dto.TaxCalculatorDto;
import com.visitor.calculator.mapstruct.CalculationMapstruct;
import com.visitor.calculator.model.calculator.CalculatorResultResponse;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;
import com.visitor.calculator.repository.CalculationRepository;
import com.visitor.calculator.service.CalculatorService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService{
    private Calculation calculation;
    private ApplicationContext applicationContext;
    private CalculationMapstruct mapstruct;
    private CalculationRepository repository;

    public CalculatorServiceImpl(Calculation calculation, ApplicationContext applicationContext, CalculationMapstruct mapstruct, CalculationRepository repository) {
        this.calculation = calculation;
        this.applicationContext = applicationContext;
        this.mapstruct = mapstruct;
        this.repository = repository;
    }

    @Override
    public CalculatorResponse calculate(BmiCalculatorDto request) {
        CalculatorResultResponse calculatorResultResponse = mapstruct.mapToBmiResponse(request);
        Calculator calculator = repository.save(mapstruct.mapBmiCalculatorDtoToCalculator(request, calculatorResultResponse));
        return mapstruct.mapToCalculatorResponse(calculator);
    }

    @Override
    public CalculatorResponse calculate(TaxCalculatorDto request) {
        CalculatorResultResponse calculatorResultResponse = mapstruct.mapToTaxResponse(request);
        Calculator calculator = repository.save(mapstruct.mapTaxCalculatorDtoToCalculator(request,calculatorResultResponse));
        return mapstruct.mapToCalculatorResponse(calculator);
    }

}
