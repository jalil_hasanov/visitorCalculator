package com.visitor.calculator.service;


import com.visitor.calculator.dto.BmiCalculatorDto;
import com.visitor.calculator.dto.TaxCalculatorDto;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;

public interface CalculatorService {
    CalculatorResponse calculate(BmiCalculatorDto request);
    CalculatorResponse calculate(TaxCalculatorDto request);
}
