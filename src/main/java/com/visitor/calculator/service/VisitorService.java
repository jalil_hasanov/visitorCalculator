package com.visitor.calculator.service;

import com.visitor.calculator.dto.VisitorDto;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.model.visitor.VisitorListResponse;
import com.visitor.calculator.model.visitor.VisitorResponse;

import java.util.List;

public interface VisitorService {
    VisitorResponse create(VisitorDto visitor);

    VisitorListResponse findAll();

    VisitorResponse findById(Long id);
}
