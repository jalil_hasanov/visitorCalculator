package com.visitor.calculator.controller;

import com.visitor.calculator.dto.VisitorDto;
import com.visitor.calculator.model.visitor.Visitor;
import com.visitor.calculator.model.visitor.VisitorListResponse;
import com.visitor.calculator.model.visitor.VisitorResponse;
import com.visitor.calculator.service.VisitorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/visitor")
@AllArgsConstructor
public class VisitorController {
    private VisitorService service;

    @PostMapping("/create")
    public ResponseEntity<VisitorResponse> create(@RequestBody VisitorDto visitor){
        return ResponseEntity.status(HttpStatus.OK).body(service.create(visitor));
    }

    @GetMapping("/find/all")
    public ResponseEntity<VisitorListResponse>findAll(){
        return ResponseEntity.status(HttpStatus.OK).body(service.findAll());
    }

    @GetMapping("find/{id}")
    public ResponseEntity<VisitorResponse>findById(@PathVariable("id")Long id){
        return ResponseEntity.status(HttpStatus.OK).body(service.findById(id));
    }
}
