package com.visitor.calculator.controller;

import com.visitor.calculator.dto.BmiCalculatorDto;
import com.visitor.calculator.dto.TaxCalculatorDto;
import com.visitor.calculator.model.calculator.Calculator;
import com.visitor.calculator.model.calculator.CalculatorResponse;
import com.visitor.calculator.service.CalculatorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculator")
@AllArgsConstructor
public class CalculatorController {
    private CalculatorService service;

    @PostMapping("/bmi")
    public ResponseEntity<CalculatorResponse> bmiCalculator(@RequestBody BmiCalculatorDto request){
       return ResponseEntity.status(HttpStatus.OK).body(service.calculate(request));
    }

    @PostMapping("/tax")
    public ResponseEntity<CalculatorResponse> taxCalculator(@RequestBody TaxCalculatorDto request){
        return ResponseEntity.status(HttpStatus.OK).body(service.calculate(request));
    }
}
