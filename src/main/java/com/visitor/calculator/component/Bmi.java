package com.visitor.calculator.component;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@Scope("prototype")
@Lazy(value = true)
@Builder
public class Bmi implements Calculable{
    Double mass;
    Double height;

    @Override
    public Double accept(Calculation calculation) {
        return calculation.calculate(this);
    }
}
