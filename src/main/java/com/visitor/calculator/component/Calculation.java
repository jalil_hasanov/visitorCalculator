package com.visitor.calculator.component;

public interface  Calculation {
    Double calculate(Tax tax);
    Double calculate(Bmi bmi);
}
