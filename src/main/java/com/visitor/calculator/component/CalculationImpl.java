package com.visitor.calculator.component;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

@Component
public class CalculationImpl implements Calculation{
    @Override
    public Double calculate(Tax request) {
        Double price = request.getPrice();
        Double taxRate = request.getTaxRate();
        Integer additionalTax = request.getAdditionalTax();
        Double result = (price*taxRate+((price+additionalTax)*0.18))/100;
        String res = new DecimalFormat("#.##").format(result);
        return Double.parseDouble(res);
    }

    @Override
    public Double calculate(Bmi request) {
        Double mass = request.getMass();
        Double height = request.getHeight();
        if (height<1 || mass==0) throw new IllegalArgumentException();
        Double result = mass/Math.pow(height,2);
        String res = new DecimalFormat("#.##").format(result);
        return Double.parseDouble(res);
    }
}
