package com.visitor.calculator.component;

public interface Calculable {
    Double accept(Calculation calculation);
}
