package com.visitor.calculator.component;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Scope("prototype")
@Builder
public class Tax implements Calculable{
    Double price;
    Double taxRate;
    Integer additionalTax;

    @Override
    public Double accept(Calculation calculation) {
        return calculation.calculate(this);
    }
}
