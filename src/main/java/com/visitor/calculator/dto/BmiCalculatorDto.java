package com.visitor.calculator.dto;

import com.visitor.calculator.enums.CalculatorType;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BmiCalculatorDto {
    Double mass;
    Double height;
    String type;
    Long visitorId;
}
