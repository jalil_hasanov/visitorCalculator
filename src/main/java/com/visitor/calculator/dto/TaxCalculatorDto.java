package com.visitor.calculator.dto;

import com.visitor.calculator.enums.CalculatorType;
import lombok.*;
import lombok.experimental.FieldDefaults;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaxCalculatorDto {
    Double price;
    Double taxRate;
    Integer additionalTax;
    String type;
    Long visitorId;
}
