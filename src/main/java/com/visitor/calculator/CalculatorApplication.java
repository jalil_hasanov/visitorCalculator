package com.visitor.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class CalculatorApplication implements CommandLineRunner {
	@Autowired
	private ApplicationContext appContext;

	public static void main(String[] args) {

		ConfigurableApplicationContext context = SpringApplication.run(CalculatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
