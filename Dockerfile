FROM openjdk:11 as tetema
COPY build/libs/calculator-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java","-jar", "calculator-0.0.1-SNAPSHOT.jar"]
CMD ["-jar","/app/calculator-0.0.1-SNAPSHOT.jar"]